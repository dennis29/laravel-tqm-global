	<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('/', function () {
    return view('welcome');
});


*/


Route::get('/', 'UserController@index')->name('login');

Route::get('/register', 'UserController@register')->name('register');

Route::get('/bantuan', 'UserController@bantuan')->name('bantuan');

Route::post('/login', 'ZoneController@login')->name('to_login');


Route::post('/to_register', 'UserController@to_register')->name('to_register');

Route::get('/Zone', 'ZoneController@Zone')->name('Zone');
	
Route::post('/AddUserAssociate', 'UserAssociateController@add_user_associates')->name('AddUserAssociate');
	
Route::post('/update_all_zones', 'ZoneController@update_all_zones')->name('update_all_zones');
	
Route::post('/update_all_zones_option', 'ZoneController@update_all_zones_option')->name('update_all_zones_option');
	
Route::post('/update_zone_title', 'ZoneController@update_zone_title')->name('update_zone_title');
	
Route::post('/zone_user_associates', 'UserAssociateController@zone_user_associates')->name('zone_user_associates');
	
Route::get('/AllZone', 'ZoneController@AllZone')->name('AllZone');
	
Route::post('/AddZone', 'ZoneController@add_zone')->name('AddZone');
	
Route::post('/UpdateSetting', 'UserController@UpdateSetting')->name('UpdateSetting');
	
Route::post('/layout_zone', 'ZoneController@layout_zone')->name('layout_zone');

Route::get('get_by_id_Guest', ['as' => 'get_by_id_Guest', 'uses' => 'ZoneController@get_by_id_Guest']);
	
Route::post('save_data', 'ZoneController@save_data')->name('save_data_post');

Route::get('save_data', 'ZoneController@save_data')->name('save_data');
	
Route::post('update_zone_name', 'ZoneController@update_zone_name')->name('update_zone_name');

Route::post('get_by_id', ['as' => 'get_by_id', 'uses' => 'ZoneController@get_by_id']);


Route::get('/Zone_Associate', 'ZoneController@Zone_Associate')->name('Zone_Associate');

Route::get('/AllZoneAssociate', 'ZoneController@AllZoneAssociate')->name('AllZoneAssociate');

Route::get('get_by_id_Guest_Ass', ['as' => 'get_by_id_Guest_Ass', 'uses' => 'ZoneController@get_by_id_Guest_Ass']);

Route::post('/logout', 'UserController@logout')->name('logout');

Route::get('/kembali', 'UserController@kembali')->name('kembali');