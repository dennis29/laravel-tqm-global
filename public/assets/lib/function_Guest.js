function load_body(zone_id){
	  
	$.ajax({
	type: "GET",	
	url:"get_by_id_Guest_Ass", 
    data: { id: zone},
	beforeSend: function() {
			var options = {
				 theme:"sk-cube-grid",
				 message:'Mohon tunggu',
				 backgroundColor:"#1847B1",
				 textColor:"white"
			};
			HoldOn.open(options);
			
	},	
	complete: function(xhr){
			HoldOn.close();
			if (xhr.status == 304){
					window.location = '/';
			}
	},
    success:function(data) {
		if(data)
		{
			if(data.data)
			{
				$('#table_1').html(data.data);
			}
		}
			
	  }
	})
	.done(function( data ) {
		
		update_all_zones();
		update_zone_title();
	})
}

function save_body(zone_id,zone_to){
	
if(!zone_id)
{
	zone_id=zone;
}	


	$.ajax({
		url:"save_data", 
		type: "POST",	
        data: {	
			body: $('#table_1').html(),
			id: zone_id,
			"_token": $('input[name=_token]').val(),
		},
		beforeSend: function() {
			var options = {
				 theme:"sk-cube-grid",
				 message:'Mohon tunggu',
				 backgroundColor:"#1847B1",
				 textColor:"white"
			};
			HoldOn.open(options);
			
		},
		complete: function(xhr){
			HoldOn.close();
			if (xhr.status == 304){
					window.location = '/';
			}
		},
        success: function (response) {
	//		alert('Save Successfuly') 
			
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
	
}
	
load_body(zone);


function change_image(zone,arr=[]){	

	for (i = 0; i < arr.length; i++) {
	  if(document.getElementById(arr[i]).className  == "green")
	  {
		document.getElementById(arr[i]).src = zone+"/images/"+arr[i]+"_red.jpg"; 
		document.getElementById(arr[i]).className  = "red";
	  }
	  else
	  {
		document.getElementById(arr[i]).src = zone+"/images/"+arr[i]+"_green.jpg"; 
		document.getElementById(arr[i]).className  = "green";
	  }
	  var container = document.querySelector("#"+arr[i]);
	} 
//	save_body();
}

function change_new_table_col(id=0,table_type=""){
	
			if($("#t"+id).attr('class')=="green")
			{

			   $("#t"+id).attr('class','red');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_red.jpg" width="78" height="68" alt=""><b>'+id+'</b>');


			}
			else{

			   $("#t"+id).attr('class','green');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_green.jpg" width="78" height="68" alt=""><b>'+id+'</b>');
			}
			//save_body();

}

function change_color_dm(arr=[],arr_table_type=[])
{		
	
		   var id_div = arr.join('_');
		   if($('#t'+id_div+'_merge').attr('class')=="green")
		   {
		      var div = "";
		      $('#t'+id_div+'_merge').attr('class','red');
		      for(i=0;i<arr.length;i++)
			  {
				div +='<div style="float:left;"><img class="'+arr_table_type[i]+'" src="assets/images/'+arr_table_type[i]+'_red.jpg" width="78" height="68" alt=""><b>'+arr[i]+'</b></div>';
			  }
			  $('#t'+id_div+'_merge').html(div);
		   }
		   else
		   {
		      var div = "";
		      $('#t'+id_div+'_merge').attr('class','green');
		      for(i=0;i<arr.length;i++)
			  {
				div +='<div style="float:left;"><img class="'+arr_table_type[i]+'" src="assets/images/'+arr_table_type[i]+'_green.jpg" width="78" height="68" alt="">'+arr[i]+'</div>';
			  }
			  $('#t'+id_div+'_merge').html(div);
		   }
		   //save_body();		   
   
}