
function load_body(zone_id){
	if(!zone_id)
	{
		zone_id = zone;
	}
	
	$.ajax({
	type: "GET",
	url:"get_by_id_Guest", 
    data: { id: zone_id},
	async: false,
	beforeSend: function() {
			var options = {
				 theme:"sk-cube-grid",
				 message:'Mohon tunggu',
				 backgroundColor:"#1847B1",
				 textColor:"white"
			};
			HoldOn.open(options);
			
	},	
	complete: function(xhr){
			HoldOn.close();
			if (xhr.status == 304){
					window.location = '/';
			}
	},
    success:function(data) {
		if(data)
		{
			if(data.data)
			{
				$('#table_1').html(data.data);
			//	$('img,div,span').draggable();
			}
			
		}
			
	  }
	}).done(function( data ) {
		      $('img,div,span').each(function () {

				var arrx = ['green','blue','red','green-dragging','red-dragging','blue-dragging','green ui-draggable-dragging','red ui-draggable-dragging'
				,'red ui-draggable-dragging','input-group-text','input-group-text ui-draggable' ,'green ui-draggable','red ui-draggable','blue ui-draggable'];
				var arry = ['border_red rectangle','border_green rectangle','border_blue rectangle'];
				var arrz = ['merge'];
				var i = $(this).attr('class');
				
			    if(i)
				{
 				  if(i.includes('ui-draggable'))
				  {
					$(this).attr('class',$(this).attr('class').replace(' ui-draggable',''));
				  }
				}
				if(i)
				{
				  if(i.includes('-dragging'))
				  {
					$(this).attr('class',$(this).attr('class').replace('-dragging',''));
				  }
				}
				
				var i = $(this).attr('class');
				
				if(arrx.includes(i))
				{
					$(this).draggable();
					$( this ).css('z-index','1');
				
					$(this).mouseover(function(){
						$( this ).css('z-index','11');
					});
				}
				
				if(arrz.includes(i))
				{
					$(this).draggable();
				}
				
				if(arry.includes(i))
				{
					$(this).click(function() {
						if($(this).attr('class')=="border_blue rectangle ui-draggable")
						{
									
						   $(this).attr('class','border_red rectangle ui-draggable');
										  
						}
						else if($(this).attr('class')=="border_green rectangle ui-draggable")
						{
									
						   $(this).attr('class','border_blue rectangle ui-draggable');
										  
						}
						else{
						  
						   $(this).attr('class','border_green rectangle ui-draggable');
						}		
						
						
					});
					$(this).draggable();
				}
			
			});

	}).done(function( data ) {
		
		//update_all_zones_option();
		//update_all_zones();
		//update_zone_title();
	})
}

function save_body(zone_id){
if(!zone_id)
{
	zone_id=zone;
}	
	$.ajax({
        url:"save_data", 
		async: false,
        type: "post",
		beforeSend: function() {
			var options = {
				 theme:"sk-cube-grid",
				 message:'Mohon tunggu',
				 backgroundColor:"#1847B1",
				 textColor:"white"
			};
			HoldOn.open(options);
			
		},
		complete: function(xhr){
			HoldOn.close();
			if (xhr.status == 304){
					window.location = '/';
			}
		},
        data: {	
			body: $('#table_1').html(),
			id: zone_id,
			"_token": $('input[name=_token]').val(),
		},
		error: function (request, status, error) {
			window.location = '/';
		},
        success: function (response) {
			
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
}


function update_zone_name(zone_id){
if(!zone_id)
{
	zone_id=zone;
}	
	$.ajax({
        url:"update_zone_name", 
		async: false,
        type: "post",
		beforeSend: function() {
			var options = {
				 theme:"sk-cube-grid",
				 message:'Mohon tunggu',
				 backgroundColor:"#1847B1",
				 textColor:"white"
			};
			HoldOn.open(options);
		},
		complete: function(xhr){
			//HoldOn.close();
			if (xhr.status == 304){
					window.location = '/';
			}
		},
        data: {	
			zone_name: $('#zone_title').val(),
			id: zone_id,
			"_token": $('input[name=_token]').val(),
		},
		
        success: function (data) {
     			location.reload(); 
			
        }
    });
}

load_body(zone);
		 
function change_image(zone,arr=[]){				   
                  for (i = 0; i < arr.length; i++) {
					if(document.getElementById(arr[i]).className  == "blue")
					{
							document.getElementById(arr[i]).src = "/"+zone+"/images/"+arr[i]+"_red.jpg"; 
							document.getElementById(arr[i]).className  = "red";
					}
					else if(document.getElementById(arr[i]).className  == "green")
					{
							document.getElementById(arr[i]).src = "/"+zone+"/images/"+arr[i]+"_blue.jpg"; 
							document.getElementById(arr[i]).className  = "blue";
					}
					else
					{
							document.getElementById(arr[i]).src = "/"+zone+"/images/"+arr[i]+"_green.jpg"; 
							document.getElementById(arr[i]).className  = "green";
					}

				  }    

}

function redirect_to(zoneto){
	  save_body(zone,zoneto);
} 

//$('#zone_title').attr('readonly',true);	
var udah_click = 1;
$("#zone_title").click(function() {
	if($('#zone_title').attr('readonly') == false)
	{
		 $('#zone_title').attr('readonly',"readonly");
		  
	}
	else
	{
		  $('#zone_title').attr('readonly',false);
	}
	  
	$('#zone_title').attr('readonly') == false;
	$('#ubah_title').css('width','132px');
	$('#zone_title').css('width','795px');
	$('#ubah_title').css('display','inline');
});		

$("#ubah_title").click(function() {
   $('#zone_title').attr('readonly',true);
   $('#zone_title').css('width','666px');
   $('#ubah_title').css('display','none');
   update_zone_name();
});