var once = 1;
function load_body(){
	
	$.ajax({
	type: "POST",	
    url:"get_by_id", 
    data: { 
		zones : zones,
		"_token": $('input[name=_token]').val()
	},
	beforeSend: function() {
		   if(once == 1)
		   {
			 var options = {
				 theme:"sk-cube-grid",
				 message:'Mohon tunggu',
				 backgroundColor:"#1847B1",
				 textColor:"white"
			 };
			 HoldOn.open(options);   
		   };once++;
	},	
	complete: function(xhr){
			HoldOn.close();
			if (xhr.status == 304){
					window.location = '/';
			}
	},
    success:function(data) {
    	if(data)
		{
			var count = 0;
			$.each(data.data, function( index, value ) {
				 if(value.body !== '')
			   	 {
					$('#table_'+index).html(value.body);
				 }
				 $('#zone_title_'+index).val(data.all_zone_name[count]['zone_name']);
				 console.log(index);
				 console.log(data.all_zone_name[count]['zone_name']);
				 count++;
		    });
	    }		
	  }
	})
	
	
}

function save_body(zone_id,zone_to){
	
if(!zone_id)
{
	zone_id=zone;
}	

$.ajax({
     	url:"save_data", 
        type: "POST",
        data: {	
			body: $('#table_'+zone_id).html(),
			id: zone_id,
			"_token": $('input[name=_token]').val(),
		},
		beforeSend: function() {
			$('.btn').attr('class','btn btn-primary fa fa-spinner fa-spin');
		},
		complete: function(){
			$('.btn').attr('class','btn btn-primary');
		},
        success: function (response) {
			//window.location.reload(); 
			
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
	});
	
	console.log(zones);
	
}

load_body();
		 
function change_image(zone,arr=[]){		
  		   
                  for (i = 0; i < arr.length; i++) {
					if(document.getElementById(arr[i]).className  == "blue")
					{
							document.getElementById(arr[i]).src = zone+"/images/"+arr[i]+"_red.jpg"; 
							document.getElementById(arr[i]).className  = "red";
					}
					else if(document.getElementById(arr[i]).className  == "green")
					{
							document.getElementById(arr[i]).src = zone+"/images/"+arr[i]+"_blue.jpg"; 
							document.getElementById(arr[i]).className  = "blue";
					}
					else
					{
							document.getElementById(arr[i]).src = zone+"/images/"+arr[i]+"_green.jpg"; 
							document.getElementById(arr[i]).className  = "green";
					}

				  }    
				  
}

function change_new_table_col(id=0,table_type=""){
			if($("#t"+id).attr('class')=="blue")
			{

			   $("#t"+id).attr('class','red');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_red.jpg" width="78" height="68" alt="">'+id);


			}
			
			else if($("#t"+id).attr('class')=="red")
			{

			   $("#t"+id).attr('class','green');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_green.jpg" width="78" height="68" alt="">'+id);


			}
			
			else{

			   $("#t"+id).attr('class','blue');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_blue.jpg" width="78" height="68" alt="">'+id);
			}
			
			


}

function change_color_dm(arr=[],arr_table_type=[]){

   var id_div = arr.join('_');
    if($('#t'+id_div+'_merge').attr('class')=="green")
		   {
		      var div = "";
		      $('#t'+id_div+'_merge').attr('class','blue');
		      for(i=0;i<arr.length;i++)
			  {
				  console.log(arr_table_type[i]);
				div +='<div style="float:left;"><img class='+JSON.stringify(arr_table_type[i])+' src="assets/images/'+arr_table_type[i]+'_blue.jpg" width="78" height="68" alt="">'+arr[i]+'</div>';
			  }
			  $('#t'+id_div+'_merge').html(div);
		   }
		   else if($('#t'+id_div+'_merge').attr('class')=="blue")
		   {
		      var div = "";
		      $('#t'+id_div+'_merge').attr('class','red');
		      for(i=0;i<arr.length;i++)
			  {
				div +='<div style="float:left;"><img class='+JSON.stringify(arr_table_type[i])+' src="assets/images/'+arr_table_type[i]+'_red.jpg" width="78" height="68" alt="">'+arr[i]+'</div>';
			  }
			  $('#t'+id_div+'_merge').html(div);
		   }
		   else
		   {
		      var div = "";
		      $('#t'+id_div+'_merge').attr('class','green');
		      for(i=0;i<arr.length;i++)
			  {
				div +='<div style="float:left;"><img class='+JSON.stringify(arr_table_type[i])+' src="assets/images/'+arr_table_type[i]+'_green.jpg" width="78" height="68" alt="">'+arr[i]+'</div>';
			  }
			  $('#t'+id_div+'_merge').html(div);
		   }	   
		   
		   

}
//hide save button in all zone
localStorage['item'] = 3;

timerW();

function timerW()
{
	var interval_to_save = setInterval(function () {
    localStorage['item']--;
    console.log(localStorage['item']);
    if (localStorage['item'] == 0) {
		clearInterval(interval_to_save);
		load_body(); 
		localStorage['item'] = 3;
		timerW();
    }
}, 1000);
}