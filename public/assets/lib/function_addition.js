$("#form_add_zone").submit(function(event){
    event.preventDefault();
	var form = $(this);
	if($('#form_add_zone #zone_name').val().replace(/(\r\n|\n|\r)/gm, "").replace(/\s+/g, '') == ''){
		
		alert("Masukkan Nama Zona");
		$('#form_add_zone #zone_name').focus();
		return false;
	}
    $.ajax({
           type: "POST",
		   async: false,
           url:  '/AddZone',
           data: form.serialize(), // serializes the form's elements.
		   beforeSend: function() {
			var options = {
				 theme:"sk-cube-grid",
				 message:'Mohon tunggu',
				 backgroundColor:"#1847B1",
				 textColor:"white"
			};
			HoldOn.open(options);
		   },
		   complete: function(xhr) { 
				HoldOn.close(); 
				if (xhr.status == 304){
					window.location = '/';
				}
		   },
           success: function(data)
           {
			    window.location = '';

           }
    });
});	


$("#form_update_setting").submit(function(event){
    event.preventDefault();
	var form = $(this);

    $.ajax({
           type: "POST",
		   async: false,
           url:  '/UpdateSetting',
           data: form.serialize(), // serializes the form's elements.
		   beforeSend: function() {
			var options = {
				 theme:"sk-cube-grid",
				 message:'Mohon tunggu',
				 backgroundColor:"#1847B1",
				 textColor:"white"
			};
			HoldOn.open(options);
		   },
		   complete: function(xhr) { 
		        HoldOn.close(); 
				if (xhr.status == 304){
					window.location = '/';
				}
		   },
           success: function(data)
           {	
			   if(data)
			   {
				   document.getElementById('logout-form').submit();
			   }
			   else
			   {
				   $('#fails').attr('style','display:block');
				   $("#form_update_setting #username").focus();
				   setTimeout(function(){
					 $('#fails').attr('style','display:none');
				   }, 2000)
			   }

           }
    });
});	

$("#form_add_user_associate").submit(function(event){
    event.preventDefault();
	var form = $(this);

    $.ajax({
           type: "POST",
		   async: false,
           url:  '/AddUserAssociate',
           data: form.serialize(), // serializes the form's elements.
		   beforeSend: function() {
			var options = {
				 theme:"sk-cube-grid",
				 message:'Mohon tunggu',
				 backgroundColor:"#1847B1",
				 textColor:"white"
			};
			HoldOn.open(options);
		   },
		   complete: function(xhr) { 
		        HoldOn.close(); 
				if (xhr.status == 304){
					window.location = '/';
				}
		   },
           success: function(data)
           {
			   $('#fails_add_user_assc').attr('style','display:block');   
			   if(data.success) {
				   $('#form_add_user_associate').trigger("reset");
				   $('#fails_add_user_assc').html("Berhasil Tambah Data");
				   window.location = '';
			   }
			   else{
				   $('#fails_add_user_assc').html("Username sudah ada. Coba username berbeda"); 
				   $("#form_add_user_associate #username").focus();
			   }
			   setTimeout(function(){
					 $('#fails_add_user_assc').attr('style','display:none');
				   }, 2000);	   
           }
    });
});

function update_all_zones(){
	
	$.ajax({
           type: "POST",
		   async: false,
           url:  '/update_all_zones',
           data: {"_token": $('input[name=_token]').val()}, // serializes the form's elements.
		   complete: function(xhr) { 
				if (xhr.status == 304){
					window.location = '/';
				}
		   },
           success: function(data)
           {   
			   var li = '<a href="AllZone" class="dropdown-item">Semua Zona</a>';
				$.each(data.data, function( index, value ) {
				  li += '<a href="'+data.url+'?zone_id='+value.id+'" class="dropdown-item">'+ucwords(value.zone_name)+'</a>'	
				});
				li += '<a href="#" data-toggle="modal" data-target="#modal_popup" id="add_zone" class="dropdown-item"><i class="fa fa-square-o" aria-hidden="true"></i> Tambah</a>';
				$('#item_dropdown').html(li);   
           }
    });
	
}

function update_all_zones_option(){
	
	$.ajax({
           type: "POST",
		   async: false,
           url:  '/update_all_zones_option',
           data: {"_token": $('input[name=_token]').val()}, // serializes the form's elements.
		   complete: function(xhr) { 
				if (xhr.status == 304){
					window.location = '/';
				}
		   },
           success: function(data)
           {
			   var data_option = '<option value="">== Pilih Zona ==</option>';
			   $.each(data.data, function( index, value ) {
				  data_option += '<option value="'+value.id+'">'+ucwords(value.zone_name)+'</option>'	
				});
			   $('#form_add_user_associate #zone_id').html(data_option);
           }
    });
}

function update_zone_title(){
	$.ajax({
           type: "POST",
		   async: false,
           url:  '/update_zone_title',
           data: {id:zone,"_token": $('input[name=_token]').val()},
		   complete: function(xhr) { 
				if (xhr.status == 304){
					window.location = '/';
				}
		   },
           success: function(data)
           {
			 if($('#zone_title').attr('readonly') == 'readonly')
			 {
				$('#zone_title').val(ucwords(data));
			 }
           }
    });
}

function ucwords(str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}