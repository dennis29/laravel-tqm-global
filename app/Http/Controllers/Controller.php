<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use App\zone;
use Redirect;

use Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	public function protected_application($request,$allzone='')
	{
		$check_zone_permission_owner =  $request->session()->get('id');		
	
		if($check_zone_permission_owner)
		{
			if($this->protected_from_unauthorize($request,$allzone))
			{
			   return true;	
			}
				
			else
			{
			   return false;	
			}
				
		}
		else
		{
			if($this->protected_from_unauthorize_assoc_user($request,$allzone))
			{
			   return true;	
			}
				
			else
			{
			   return false;	
			}
		}
		
	}
	
	
	public function protected_from_unauthorize($request,$allzone='')
	{
		
		$check_zone_permission =  $request->session()->get('id');
		
		$zone = new Zone();
		
		if(!$check_zone_permission){	
		    $request->session()->flush();
			return false;
		}
		
		$get_zone['zone_id'] = Input::get('id');		
		
		//dd($zone->check_zone($get_zone['zone_id'],$request->session()->get('id')));
		if($get_zone['zone_id'] && !$allzone && $check_zone_permission)
		{
			
			//dd($zone->check_zone($get_zone['zone_id'],$request->session()->get('id')));
			if(!($zone->check_zone($get_zone['zone_id'],$check_zone_permission))){
				
				$request->session()->flush();
				return false;
			}
		}
		return true;
	}
	
	public function protected_from_unauthorize_assoc_user($request,$allzone)
	{
		if($allzone){
			$check_zone_permission_ass = $request->session()->get('all_zone_associate');
			if(!$check_zone_permission_ass)
			{
				$request->session()->flush();
				return false;
			}
		}
		else
		{ 
	        $check_zone_permission_ass  = $request->session()->get('id_assoc');
			if(!$check_zone_permission_ass)
			{
				$request->session()->flush();
				return false;
			}
		}
		
		return true;	
	}
}