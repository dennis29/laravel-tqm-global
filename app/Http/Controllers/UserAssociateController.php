<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\user_associate;

use App\user;

use App\zone;

use Session;

class UserAssociateController extends Controller
{
    public function add_user_associates(Request $request)
	{
		$this->protected_application($request);
		$user_associate = new User_associate();
		$zone = new Zone;
		$user = new User();
		$data = array();
		$get_data = $user->cek_username($request->post());
		if($get_data)
		{
			$data['success'] = false;
		}
		else
		{
			$data['success'] = $user_associate->add_user_associates($request->post(),$request->session()->get('id'));
		}
		$data['data'] = $zone->get_all_zone_to_associate($request->session()->get('id'));
		Session::save();
		return response()->json($data);
	}
	
	public function zone_user_associates(Request $request)
	{
		$this->protected_application($request);
		$zone = new Zone;
		$data['data'] = $zone->get_all_zones($request->session()->get('id'));
		$data['url'] = route('Zone');
		Session::save();
		return response()->json($data);
	}
}