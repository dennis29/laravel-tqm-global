<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\user;

use App\zone;

use Illuminate\Support\Facades\Input;

use Session;

use Auth;

use Redirect;

use Response;

class ZoneController extends Controller
{
    	
	public function login(Request $request)
	{	
	    $request->session()->flush();
		$user = new User();
		$zone = new Zone();
		$get_data = $user->login($request->post());
	//	dd($get_data);
		if($get_data)
		{
			if($get_data[0]->id)
			{	
		        if($get_data[0]->user_associate)
				{
					$request->session()->put('username', $request->post('username'));
					$request->session()->put('id_owner', $get_data[0]->id);
					$request->session()->put('company_name', $get_data[0]->company_name);
					if($get_data[0]->is_allzone)
					{
						$request->session()->put('all_zone_associate', 'allzone');
						return redirect('/AllZoneAssociate');	
					}
					else{
						$request->session()->put('zone_associate', $get_data[0]->zone_associate);
						$request->session()->put('id_assoc', $get_data[0]->user_associate);
						return redirect('/Zone_Associate');	
					}
				}
				else
				{
					$request->session()->put('username', $request->post('username'));
					$request->session()->put('id', $get_data[0]->id);
					$request->session()->put('company_name', $get_data[0]->company_name);
					return redirect('/Zone?zone_id='.$zone->get_first_zone($get_data[0]->id)->id);
				}
			}
			else
			{
			    return Redirect::back()->withErrors(['Username atau Password salah/tidak terdaftar']);
			}
		}
		else
		{
			return Redirect::back()->withErrors(['Username atau Password salah / tidak terdaftar']);
		}
	
	}
	
	public function Zone(Request $request)
	{
		if(!$this->protected_from_unauthorize($request,''))
		{
			$request->session()->flush();
			return redirect('/');
		}
		$zone = new Zone();
		$get_zone['zone_id'] = Input::get('zone_id');	
		$get_zone['zone_name'] = $zone->get_zone_name($get_zone['zone_id'])->zone_name;
		$get_zone['zones'] = $zone->get_all_zones($request->session()->get('id'));
		$get_zone['zone_to_associate'] = $zone->get_all_zone_to_associate($request->session()->get('id'));
		Session::save();
		return view('Zone/index',compact('get_zone'));
	}
	
	public function AllZone(Request $request)
	{
		if(!$this->protected_from_unauthorize($request,''))
		{
			$request->session()->flush();
			return redirect('/');
		}
		
		$zone = new Zone();
		$get_zone['zones'] = $zone->get_all_zones($request->session()->get('id'));
		$get_zone['zone_to_associate'] = $zone->get_all_zone_to_associate($request->session()->get('id'));
		Session::save();
		return view('AllZone/index',compact('get_zone'));
	}
	
	
	public function Zone_Associate(Request $request)
	{
		if(!$this->protected_from_unauthorize_assoc_user($request,''))
		{
			$request->session()->flush();
			return redirect('/');
		}
		
		$zone = new Zone();
		$get_zone['zone_name'] = $zone->get_zone_name($request->session()->get('zone_associate'))->zone_name;
		Session::save();
		return view('Zone_Associate/index',compact('get_zone'));
		
	}
	
	public function AllZoneAssociate(Request $request)
	{
		if(!$this->protected_from_unauthorize_assoc_user($request,'allzone'))
		{
			$request->session()->flush();
			return redirect('/');
		}
		$zone = new Zone();
		$get_zone['zones'] = $zone->get_all_zones($request->session()->get('id_owner'));
		Session::save();
		return view('AllZone/index',compact('get_zone'));
	}
	
	public function get_by_id_Guest(Request $request)
	{
		if(!$this->protected_application($request,''))
		{
			$request->session()->flush();
			return Response::json([], 304); // Status code here
		}
			
		
		$user = new User();
		
		$zone = new Zone();
		
		$data['data'] = $user->get_by_id_Guest(Input::get('id'),$request);
		
		Session::save();
		
		return response()->json($data);
	
	}
	
	public function get_by_id(Request $request)
	{
		if(!$this->protected_application($request,'allzone'))
		{
			$request->session()->flush();
			return Response::json([], 304); // Status code here
		}
		
		$user = new User();
		
		$zone = new Zone();
		
		Session::save();
		
		$data['data'] = $user->get_by_id(Input::get(),$request);
		
		$id = $request->session()->get('id_owner');
		
		if(!$id){
			
			$id = $request->session()->get('id');
		}
		
		$data['all_zone_name'] = $zone->get_all_zones($id);
			
		return response()->json($data);
	
	}
	
	public function save_data(Request $request)
	{
		if(!$this->protected_application($request,''))
		{
			$request->session()->flush();
			return Response::json([], 304); // Status code here
		}
		
		$user = new User();
		Session::save();
		return response()->json($user->save_data($request->post(),$request));	
	}
	
	public function update_zone_name(Request $request)
	{
		if(!$this->protected_application($request,''))
		{
			$request->session()->flush();
			return Response::json([], 304); // Status code here
		}
		
		
		$zone = new Zone();
		Session::save();
		return response()->json($zone->update_zone_name($request->post()));
	}
	
	public function update_all_zones(Request $request)
	{
		if(!$this->protected_application($request,''))
		{
			$request->session()->flush();
			return Response::json([], 304); // Status code here
		}
		
		$zone = new Zone();
		$data['url'] = route('Zone');
		$data['data'] = $zone->get_all_zones($request->session()->get('id'));
		Session::save();
		return response()->json($data);
	}
	
	public function update_all_zones_option(Request $request)
	{
		if(!$this->protected_application($request,''))
		{
			$request->session()->flush();
			return Response::json([], 304); // Status code here
		}
		$zone = new Zone();
		$data['data'] = $zone->get_all_zone_to_associate($request->session()->get('id'));
		Session::save();
		return response()->json($data);
	}
	
	public function update_zone_title(Request $request)
	{
		if(!$this->protected_application($request,''))
		{
			$request->session()->flush();
			return Response::json([], 304); // Status code here
		}
		$zone = new Zone();
		Session::save();
		return response()->json($zone->get_zone_name(Input::get('id'))->zone_name);
	}
	
	public function add_zone(Request $request)
	{
		if(!$this->protected_application($request,''))
		{
			$request->session()->flush();
			return Response::json([], 304); // Status code here
		}
		$zone = new Zone();
		$zone->add_zone($request->post(),$request);
		$data['data'] = $zone->get_all_zones($request->session()->get('id'));
		$data['url'] = route('Zone');
		Session::save();
		return response()->json($data);	
	}
	
	public function layout_zone(Request $request)
	{
		if(!$this->protected_application($request,''))
		{
			$request->session()->flush();
			return Response::json([], 304); // Status code here
		}
		$zone = new Zone();
		$data['data'] = $zone->get_all_zones($request->session()->get('id'));
		$data['url'] = route('Zone');
		Session::save();
		return response()->json($data);	
	}
	
	public function get_by_id_Guest_Ass(Request $request)
	{
		if(!$this->protected_application($request))
		{
			$request->session()->flush();
			return Response::json([], 304); // Status code here
		}
		
		$user = new User();
		
		$data['data'] = $user->get_by_id_Guest_Ass($request);
			
		Session::save();
		
		return response()->json($data);
	
	}	
}