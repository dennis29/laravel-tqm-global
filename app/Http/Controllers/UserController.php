<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\user;

use Redirect;

use Session;

class UserController extends Controller
{
	public function index()
	{
		return view('Login/index');
	}
	
	public function register()
	{	
		return view('Register/index');
	}
	
	public function bantuan(Request $request)
	{	
	    $request->session()->flush();
		return view('Bantuan/index');
	}
	
    public function to_register(Request $request)
	{
		$request->session()->flush();
		$user = new User();
		$get_data = $user->cek_username($request->post());
		if($get_data)
		{    
	      return Redirect::back()->withErrors(['Username sudah ada. Coba username berbeda']);
		}
		else
		{
		   $user->insert_user($request->post());	
		   return  redirect('/');
		}
	
	}
	
	public function UpdateSetting(Request $request)
	{
		$this->protected_application($request);
		$user = new User();
		
		$get_data = $user->cek_username($request->post());
	
		if($get_data)
		{
			return response()->json(false);
		}
		
		Session::save();
		return response()->json($user->UpdateSetting($request->post(),$request->session()->get('id')));	
	}
	
	
	public function logout(Request $request)
	{	
		$request->session()->flush();
	
		return redirect('/');
	}
}