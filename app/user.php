<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

use File;

use Storage;

class user extends Model
{
    protected $fillable = [
        'username', 'password'
    ];
	
	public function login($array){
		
	 //DB::enableQueryLog();	
		
	 return DB::select("select id,company_name,null as user_associate,null as zone_associate,null as is_allzone  
		from users where username = '".$array['username']."' and password = '".$array['password']."'
		union all
		select users.id,company_name,user_associates.id as user_associate,user_associates.zone_id as zone_associate,is_allzone    
		from user_associates 
		join users on users.id = user_associates.user_id
		join zones on zones.id = user_associates.zone_id
		where user_associates.username = '".$array['username']."' and user_associates.password = '".$array['password']."'
		");	
	 //dd(DB::getQueryLog());
	}
	
	public function cek_username($array){
		
	//DB::enableQueryLog();	
	 return DB::select("Select username 
		from users where  username = '".$array['username']."'
		union all
		Select username 
		from user_associates where username = '".$array['username']."'
		"
		);
		//dd(DB::getQueryLog());
	}
	
	public function insert_user($post_data=array()){
		unset($post_data['_token']);
	//DB::enableQueryLog();	
	    $get_id  = DB::table('users')->insertGetId($post_data);
		$data_zone['zone_owner'] = $get_id;
		$data_zone['zone_name'] = "Zone 1";
		DB::table('zones')->insert($data_zone);
		$data_zone['zone_name'] = "Semua Zona";
		$data_zone['is_allzone'] = 1;
		return DB::table('zones')->insert($data_zone);
		//dd(DB::getQueryLog());
	}
	
	
	
	public function get_by_id($array,$request){
		$id = $request->session()->get('id');
		if(!$id)
		{
			$id = $request->session()->get('id_owner');
		}
		   foreach($array['zones'] as $k=>$v)
		   {
			 $file_loc = 'assets/zone/zone'.$v.'_'.$id.".html";
			 if(!is_file($file_loc)){
				$contents = '';           // Some simple example content.
				file_put_contents($file_loc, $contents);     // Save our content to the file.
			 }
			 $file=fopen($file_loc,"rb");
			 if(filesize($file_loc) > 0)
			   $data[$v]['body'] = fread($file,filesize($file_loc));
		     else
			   $data[$v]['body'] = "";
			 fclose($file);
		   }
		
		 return $data;
	}
	

	public function get_by_id_Guest($id,$request){
		    $file_loc = 'assets/zone/zone'.$id.'_'.$request->session()->get('id').".html";
			if(!is_file($file_loc)){
				$contents = '';           // Some simple example content.
				file_put_contents($file_loc, $contents);     // Save our content to the file.
			 }
			$data=File::get($file_loc);
		
	    	return $data;
	}
	
	public function get_by_id_Guest_Ass($request){
		    $file_loc = 'assets/zone/zone'.$request->session()->get('zone_associate').'_'.$request->session()->get('id_owner').".html";
			if(!is_file($file_loc)){
				$contents = '';           // Some simple example content.
				file_put_contents($file_loc, $contents);     // Save our content to the file.
			 }
			$data=File::get($file_loc);
		
	    	return $data;
	}
	
	public function save_data($array,$request){
		
		$id = $request->session()->get('id');
		
		if(!$id){
			$id = $request->session()->get('id_owner');
		}
		
		
		$file = @fopen('assets/zone/zone'.$array['id'].'_'.$id.".html", "r+");
		ftruncate($file, 0);
	    fwrite($file,$array['body']);
		//Storage::put('file.txt', 'Your name');
		return "";
		
	}
	
	public function UpdateSetting($post_data=array(),$id){
		
		unset($post_data['_token']);
		
		$post_data_filter = array_filter($post_data);
		
		if($post_data_filter){
		   DB::table('users')->where('id',$id)->update($post_data_filter);	
		}
		
		return true;
		//dd(DB::getQueryLog());
	}
}
