<?php

namespace App;

use DB;

use Illuminate\Database\Eloquent\Model;

class zone extends Model
{
    protected $fillable = [
        'id','zone_name','zone','zone_owner'
    ];
	
	public function get_all_zones($user_id){
		
	//DB::enableQueryLog();	
	 return DB::table('zones')
		->select(['id','zone_name'])->where('zone_owner',$user_id)->where('is_allzone',null)->orderBy('zone_name','ASC')->get();
		//dd(DB::getQueryLog());
	}
	
	public function get_all_zone_to_associate($user_id){
		
	//DB::enableQueryLog();	
	 return DB::select("select id,zone_name,is_allzone from zones where id not in (select zone_id from user_associates where user_id = '".$user_id."') order by is_allzone,zone_name ASC");
		//dd(DB::getQueryLog());
	}
	
	public function check_zone($zone_id,$user_id){
		
	//DB::enableQueryLog();	
	 return DB::table('zones')
		->select(['id'])->where('zone_owner',$user_id)->where('is_allzone',null)->where('id',$zone_id)->first();
		//dd(DB::getQueryLog());
	}
	
	public function check_zone_assoc($zone_id,$user_id){
		
	//DB::enableQueryLog();	
	 return DB::table('user_associates')
		->select(['id'])->where('id',$user_id)->where('zone_id',$zone_id)->first();
		//dd(DB::getQueryLog());
	}
	
	public function get_first_zone($user_id){
		
	//DB::enableQueryLog();	
	 return DB::table('zones')
		->select(['id'])->where('zone_owner',$user_id)->where('is_allzone',null)->orderBy('id','ASC')->first();
		//dd(DB::getQueryLog());
	}
	
	public function get_zone_associate($user_id){
		
	//DB::enableQueryLog();	
	 return DB::table('zones')
		->select(['id'])->where('zone_owner',$user_id)->orderBy('id','ASC')->first();
		//dd(DB::getQueryLog());
	}
	
	public function get_zone_name($id){
		
	 return DB::table('zones')->select(['zone_name'])->where('id',$id)->first();
	}
	
	public function update_zone_name($post_data){
		unset($post_data['_token']);
		return DB::table('zones')->where('id',$post_data['id'])->update($post_data);
		//dd(DB::getQueryLog());
	}
	
	public function add_zone($post_data=array(),$request){
		unset($post_data['_token']);
		$post_data['zone_owner'] = $request->session()->get('id');
		return DB::table('zones')->insert($post_data);
		//dd(DB::getQueryLog());
	}
	
}
