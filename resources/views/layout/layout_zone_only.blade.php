@extends('layout.layout')
@section('content')
<style>
.kiri_dikit{margin-right:760px}
</style>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="width:1350px">
<input type="text" id="zone_title" style="font-size:28px;text-align:center;height:80px;margin-left:-130px;width:665px;font-family:sans-serif" readonly="readonly" class="form-control" value="{{$get_zone['zone_name']}}">
<div id="table_1" style="float:left;margin-left:-130px;border:10px solid black;">
@yield('content_zone_only')
</div>
@include('layout.footer')
<script>
  var zone = {{Session::get('zone_associate')}};
</script>
<br/>
@endsection
@section('content_js')
 <script type="text/javascript" src="{{asset('assets/lib/function_Guest.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/lib/timer.js')}}"></script> 
@endsection