<div style="float:left;margin-left:5px;margin-top:-35px;width:800px">
<div class="container mt-4">

<ul class="nav nav-tabs responsive " role="tablist">
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#div_label" role="tab">Tambah Penanda</a>
	</li>
	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#div_add" role="tab">Tambah Meja</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#div_hapus" role="tab">Hapus Meja</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#div_gabung" role="tab">Gabung Meja</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#div_pisah" role="tab">Pisah Meja</a>
	</li>
</ul><!-- Tab panes -->
<div class="tab-content responsive">
    <div class="tab-pane" id="div_label" role="tabpanel">
		<div class="container-fluid">
			<form class="form-horizontal" id='form_label' target="#">
			    <div class="form-group col-sm-16">
				    <label class="control-label" for="label_name">Nama Penanda</label>
					<div class="form-group col-sm-16">
						<input type="text" class="form-control" style="width:250px" id="label_name" placeholder="Masukkan Nama Penanda" required>
					</div>
				</div>
			</form>
		    <div class="form-group col-sm-16">
				<div>
					<button type="submit" class="btn btn-primary" id="add_div" onclick="add_new_label()">Tambah Penanda</button>
				</div>
			</div>
		</div>
	</div> 
	<div class="tab-pane active" id="div_add" role="tabpanel" >
		<div class="container-fluid">
			<form class="form-horizontal" id='form_add' target="#">
					      <div class="form-group col-sm-16">
							  <label for="table_type">Pilih Jenis Meja :</label>
							  <select class="form-control" id="table_type" required>
								<option value="Circle_two">Meja bulat 2 bangku</option>
								<option value="Circle_four">Meja bulat 4 bangku</option>
								<option value="Square_two">Meja kotak 2 bangku</option>
								<option value="Square_four">Meja kotak 4 bangku</option>
							  </select>
						  </div>
						  <div class="form-group col-sm-16">
							<label class="control-label" for="table_no">Nomor Meja</label>
							<div>
							  <input type="text" class="form-control" id="table_no" placeholder="Masukkan Nomor Meja" required>
							</div>
						  </div>
		    </form>
			<div class="form-group col-sm-16">
				<div>
					<button type="submit" class="btn btn-primary" id="add_div" onclick="add_new_div()">Tambah Meja</button>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="div_hapus" role="tabpanel">
		<div class="container-fluid">
			<form class="form-horizontal" id='form_del'>
						  <div class="form-group col-sm-16">
							<label class="control-label" for="table_no">Nomor Meja</label>
							<div>
							  <input type="text" class="form-control" id="table_no" placeholder="Masukkan Nomor Meja">
							</div>
						  </div>
			</form>
		    <div class="form-group col-sm-16">
				<div>
				    <button type="submit" class="btn btn-primary" id="del_div" onclick="delete_div()">Hapus Meja</button>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="div_gabung" role="tabpanel">
		<div class="container-fluid">
			<form class="form-horizontal" id='form_merge'>
				<div class="form-group col-sm-16">
					<label class="control-label" for="tables_no">Masukkan Nomor Meja (dipisah dengan koma)</label>
					<div>
					    <input type="text" class="form-control" id="tables_no" value="" placeholder="Masukkan Nomor Meja">
					</div>
				</div>
			</form>
			<div class="form-group col-sm-16">
				<div>
					<button type="submit" class="btn btn-primary" id="mer_div" onclick="merge_div()">Gabung Meja</button>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="div_pisah" role="tabpanel">
		<div class="container-fluid">
			<form class="form-horizontal" id='form_split'>
				<div class="form-group col-sm-16">
					<label class="control-label" for="tables_no">Masukkan Nomor Meja di meja tergabung (dipisah dengan koma)</label>
					<div>
						<input type="text" class="form-control" id="tables_split" value="" placeholder="Masukkan Nomor Meja di meja tergabung">
					</div>
				</div>
			</form>
			<div class="form-group col-sm-16">
					<div>
						<button type="submit" class="btn btn-primary" id="spl_div" onclick="split_div()">Pisah Meja</button>
					</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
@include('layout.footer')

<style>
h4.card-title {
	margin: 0;
	font-family: "Open Sans";
	font-size: 1rem;
}

.card-header {
	padding: 0;
}
</style>

<script>
function change_color(id="")
{
	if(document.getElementById(id).className  == "border_red")
	{
		document.getElementById(id).className  = "border_green";
	}
	else if(document.getElementById(id).className  == "border_green")
	{
		document.getElementById(id).className  = "border_blue";
	}
	else
	{
		document.getElementById(id).className  = "border_red";
	}
	
	save_body();

}

var check_add = 0;
var style_x = "";
function add_new_div(id="",table_type=""){
	var tbl_type = table_type;
	if(!id)
	{
	   var id = $('#form_add #table_no').val();
	}
	if(!tbl_type)
	{
	  tbl_type = $('#table_type').val();
	}
	if(check_add > 0){
		style_x = 'style="margin-top:-60px"';
	}
	
	if(id.replace(/(\r\n|\n|\r)/gm, "").replace(/\s+/g, '') == ''){
		
		alert("Masukkan nomor meja");
		$('#form_add #table_no').focus();
		return false;
	}
	
	if($("#t"+id.replace(/[^\w\s]/gi, '')).html() != null)
	{
		alert("Meja nomor "+id.replace(/[^\w\s]/gi, '')+" sudah digunakan\n\nCoba nomor  meja yang berbeda");
		return false;
	}
	id = $('#form_add #table_no').val().replace(/[^\w\s]/gi, '');
	$('#new_table').append('<div id="t'+id+'" '+style_x+' onclick="" draggable="true" ondrag="on_drag()" class="green"><img src="assets/images/'+tbl_type+'_green.jpg" width="78" height="68" class='+JSON.stringify(tbl_type)+' /><b>'+id+'</b></div>');
	$("#t"+id).draggable();
	$('#t'+id).attr('onclick','change_new_table_col('+id+','+JSON.stringify(tbl_type)+')');
	var position = $("#t"+id).offset();
	$(window).scrollTop(position.top);	
	check_add++; 		
	save_body();
}

var label = 1;

function add_new_label(){
	if($('#form_label #label_name').val().replace(/(\r\n|\n|\r)/gm, "").replace(/\s+/g, '') == ''){
		
		alert("Masukkan nama penanda");
		$('#form_label #label_name').focus();
		return false;
	}
	
	$('#new_label').append('<div style="margin-top:-90px;"><span id="lbl'+label+'" ondrag="on_drag()" ondoubletap="on_doubletap()" class="input-group-text" style="display:inline;text-align:center;font-weight:bold;margin-top:-90px;" id="inputGroupPrepend">'+ucwords($('#label_name').val().replace(/[^\w\s]/gi, ''))+'</span></div>');
	$('#lbl'+label).draggable();
	$('#lbl'+label).focus();
	var position = $('#lbl'+label).offset();
	$(window).scrollTop(position.top);	
	label++;
	save_body();
}

var touch = 0;
function change_new_table_col(id=0,table_type=""){
touch++;
if(touch == 2)
{	
  if($("#t"+id).attr('class')=="green" || $("#t"+id).attr('class')=="green ui-draggable" || $("#t"+id).attr('class')=="green ui-draggable-dragging")
			{

			   $("#t"+id).attr('class','red');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_red.jpg" width="78" height="68" class="'+table_type+'" alt=""><b>'+id+'</b>');


			}
			else if($("#t"+id).attr('class')=="red" || $("#t"+id).attr('class')=="red ui-draggable" || $("#t"+id).attr('class')=="red ui-draggable-dragging")
			{

			   $("#t"+id).attr('class','blue');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_blue.jpg" width="78" height="68" class="'+table_type+'"  alt=""><b>'+id+'</b>');

			}
			else{

			   $("#t"+id).attr('class','green');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_green.jpg" width="78" height="68" class="'+table_type+'"  alt=""><b>'+id+'</b>');
			}
			
	//		save_body();
 touch = 0;			
}

}

function delete_div(){  
	var id = $('#form_del #table_no').val();
	
	if(id.replace(/(\r\n|\n|\r)/gm, "").replace(/\s+/g, '') == ''){
		
		alert("Masukkan nomor meja");
		$('#form_del #table_no').focus();
		return false;
	}
	
	var img = $('#t'+id).find("img");
	$('#t'+id).html('<div style="width:78px;height:68px">&nbsp</div>');
	$('#t'+id).attr('id','');
	save_body();
}

function on_drag(id){
   touch = 0;
}

function on_doubletap(id){
  save_body();
}

function split_div(){
	var arr = $('#tables_split').val().split(',');
	
	i=0;
	
	var id_div = arr.join('_');	
	
	$('#t'+id_div+'_merge img').each(function() {
	    add_new_div(arr[i],$(this).attr('class'));
		//return false;
		i++;
    });

	$('#t'+id_div+'_merge').html('<div style="width:174px;height:68px">&nbsp</div>');
	$('#t'+id_div+'_merge').attr('id','');
	
	save_body();
}


//localStorage[zone] = "";

function merge_div(){

	var arr = $('#tables_no').val().split(',').replace(/[^\w\s]/gi, '');
	
	
	var filtered = arr.filter(function (el) {
		return $("#t"+el).html() != null;
	});
//	return false;
	var result =  filtered.filter(e => e.length);
	console.log(result);	
	
	if(result.length < 2)
	{
		alert('Masukkan meja yang terdaftar');
		return false;
	}
	
	var arr_table_type = [];

	$('#tables_no').val('');

	var width = 0;

	var div="";

	for(i=0;i<arr.length;i++)
	{
		    //console.log($('#t'+arr[i]+' img').attr('class'));
	        arr_table_type[i] = $('#t'+arr[i]).children('img').attr('class');  
	        div +='<div style="float:left;" ><img class="'+	[i]+'" src="assets/images/'+$('#t'+arr[i]+' img').attr('class')+'_green.jpg" width="78" height="68" alt=""><b>'+arr[i]+'</b></div>';			
			$("#t"+arr[i]).html('<div style="width:78px;height:68px">&nbsp</div>');
			$("#t"+arr[i]).attr('id','');
	}
	var id_div = arr.join('_');

	$('#table_merge').append('<div class="green" ondrag="on_drag()" style="margin-bottom:-65px" id="t'+id_div+'_merge" ondblclick=""><b>'+div+'</b></div>');	
	
	$('#t'+id_div+'_merge').attr('ondblclick',"change_color_dm(["+arr+"],"+JSON.stringify(arr_table_type)+")");

	$('#t'+id_div+'_merge').draggable();
	
	var position = $('#t'+id_div+'_merge').offset();
	$(window).scrollTop(position.top);	
	
	save_body();

}

function change_color_dm(arr=[],arr_table_type=[]){
   var div = "";
   var id_div = arr.join('_');
   if($('#t'+id_div+'_merge').attr('class')=="green" || $('#t'+id_div+'_merge').attr('class')=="green")
   {
		      $('#t'+id_div+'_merge').attr('class','blue');
		      for(i=0;i<arr.length;i++)
			  {
				  //console.log(arr_table_type[i]);
				div +='<div style="float:left;"><img class='+JSON.stringify(arr_table_type[i])+' src="assets/images/'+arr_table_type[i]+'_blue.jpg" width="78" height="68" alt=""><b>'+arr[i]+'</b></div>';
			  }
   }
   else if($('#t'+id_div+'_merge').attr('class')=="blue" || $('#t'+id_div+'_merge').attr('class')=="blue")
   {
		     
		      $('#t'+id_div+'_merge').attr('class','red');
		      for(i=0;i<arr.length;i++)
			  {
				div +='<div style="float:left;"><img class='+JSON.stringify(arr_table_type[i])+' src="assets/images/'+arr_table_type[i]+'_red.jpg" width="78" height="68" alt=""><b>'+arr[i]+'</b></div>';
			  }
			  
   }
   else
   {
		      $('#t'+id_div+'_merge').attr('class','green');
		      for(i=0;i<arr.length;i++)
			  {
				div +='<div style="float:left;"><img class='+JSON.stringify(arr_table_type[i])+' src="assets/images/'+arr_table_type[i]+'_green.jpg" width="78" height="68" alt=""><b>'+arr[i]+'</b></div>';
			  }
   }	   
   $('#t'+id_div+'_merge').html(div);
  // save_body();  

}

function div_click(id){
	if($("#t"+id).attr('class')=="border_blue rectangle")
	{

	   $("#t"+id).attr('class','border_red rectangle');

	}
	else if($("#t"+id).attr('class')=="border_green rectangle")
	{

	   $("#t"+id).attr('class','border_blue rectangle');

	}
	else{
	   $("#t"+id).attr('class','border_green rectangle');
	}
	
	//save_body();
	
		

}
</script>