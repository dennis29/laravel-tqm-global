<center><h4>Rule</h4></center>
 <li> Double Click The Table to change Table's Color from Green to Blue, Blue to Red, and Red to Green.</li>
 <li> Move The Table to anywhere inside the border.</li>
 <li> On Add Form, Select Table's Type and choose "Circle" or "Square" and enter Table Number such as 1,A,1B,12 and click "Add" Button to create new table.</li>
 <li> On Delete Form, Enter Table Number and enter Table Number such as 1,A,1B,12 and click "Delete" Button to delete new table. This only works on a created table.</li>
 <li> On Merge Form, Enter 2 or more Tables that wants to merge (separated with comma) and press "Merge" Button to merge. This only works on created tables.</li>
 <li> On Split Form, Enter 2 or more Tables that is inside merged table to split (separated with comma) and press "Split" Button to split. This only works on merged tables.</li>