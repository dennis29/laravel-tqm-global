<html>
<head>
<title>{{strtoupper(Session::get('company_name'))}}</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<?php
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Pragma: no-cache');
header('Expires: 0');
?>
</head>
<link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/icon_company.png')}}" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="{{asset('assets/css/HoldOn.min.css')}}">
<style>
	.bs-example{
    	margin: 20px;
    }
	body{
	margin-left:200px
	}
.input{
	width:70px;
    height:50px;
	background:blue;
	font-weight: bold;
	text-align:center;
	color:black
}
.border_blue{
	border:5px solid blue;
}
.border_red{
	border:5px solid red;
}
.border_green{
	border:5px solid green;
}

.rectangle {
  height: 50px;
  width: 70px;
  background-color: #555;
  float:left;
  margin-bottom:5px;

	font-weight: bold;
	text-align:center;
	margin-right:20px;
    line-height: 45px;
  text-align: center;
	color:black
}

.rectangle_merge {
  height: 50px;
  width: 70px;
  background-color: #555;
  float:left;
  margin-bottom:5px;

	font-weight: bold;
	text-align:center;
    line-height: 45px;
  text-align: center;
	color:black
}

/* Tabs*/
section {
    padding: 60px 0;
}

.bs-example{
    	margin: 20px;
    }
body{
	margin-left:200px;
}
.dropdown-item {
 font-weight: bold;
}
</style>
</head>
<body>
<div class="bs-example">
    <nav class="navbar navbar-expand-md navbar-light bg-light" style="margin-left:-150px">
        <a href="#" class="navbar-brand">{{strtoupper(Session::get('company_name'))}}</a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
	      <div id="navbarCollapse" class="collapse navbar-collapse">
	       <ul class="nav navbar-nav">
                <li class="nav-item dropdown">
				   @if(Session::get('id'))
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Zona</a>
                    <div class="dropdown-menu" id="item_dropdown">
					   <a href="{{route('AllZone')}}" class="dropdown-item">Semua Zona</a>
					@foreach($get_zone['zones'] as $zone)
					   <a href="{{route('Zone')}}?zone_id={{$zone->id}}" class="dropdown-item">{{ucwords($zone->zone_name)}}</a>
					@endforeach
					   <a href="#" data-toggle="modal" data-target="#modal_popup" id="add_zone" class="dropdown-item"><i class="fa fa-square-o" aria-hidden="true"></i> Tambah</a>
		            </div>
				   @endif	
                </li>
            </ul>
	        <ul class="nav navbar-nav ml-auto kiri_dikit">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">{{ ucwords(Session::get('username'))}}</a>
                    <div class="dropdown-menu dropdown-menu-right">
					    @if(Session::get('id'))
					     <a href="#" data-toggle="modal" data-target="#modal_popup_add_user_associate" id="add_zone" class="dropdown-item"><i class="fa fa-user-o" aria-hidden="true"></i> Tambah User Associate</a>
					     <a href="#" data-toggle="modal" data-target="#modal_popup_change_setting" id="add_zone" class="dropdown-item"><i class="fa fa-pencil" aria-hidden="true"></i> Ubah Setting</a>
                        @endif
						 <a href="{{route('bantuan')}}" target='blank' class="dropdown-item">Bantuan</a>
						 <a class="dropdown-item"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                         </a>
				     </div>
                </li>
            </ul>
        </div>
    </nav>
</div>
@if(Session::get('id'))
<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" style="margin-right:900px;" aria-labelledby="modal_popupLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document" style="margin-right:900px;">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-square-o" aria-hidden="true"></i> Tambah Zona</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
				<form id="form_add_zone" style="font-weight:bold">
				  {{ csrf_field() }} 
				  <div class="alert alert-info" id="success" style="display:none">
					  <strong>Success Save Data!</strong>
				  </div>
				  <div class="form-group">
					<label for="zone_name">Nama Zona</label>
					<input class="form-control" id="zone_name" name="zone_name" required>
				  </div>
				  <div class="modal-footer">				
					<button type="submit" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Zona</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				  </div>
				</form>
			  </div>
			</div>
		  </div>
</div>

<div class="modal fade" id="modal_popup_change_setting" tabindex="-1" role="dialog" aria-labelledby="modal_popupLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document" style="margin-right:800px;" >
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-pencil" aria-hidden="true"></i> Ubah Setting</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
				<form id="form_update_setting" style="font-weight:bold">
				  {{ csrf_field() }} 
				  <div class="alert alert-warning" id="fails" style="display:none">
					  <strong>Username sudah ada. Mohon diganti!</strong>
				  </div>
				  <div class="form-group">
					<label for="username">Username</label>
					<input class="form-control" id="username" name="username">
				  </div>
				  <div class="form-group">
					<label for="company_name">Nama Perusahaan</label>
					<input class="form-control" name="company_name">
				  </div>
				  <div class="form-group">
					<label for="password">Password</label>
					<input class="form-control" type="password" name="password">
				  </div>
				  <div class="modal-footer">				
					<button type="submit" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> Ubah Setting</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				  </div>
				</form>
			  </div>
			</div>
		  </div>
</div>
<div class="modal fade" id="modal_popup_add_user_associate" tabindex="-1" role="dialog" aria-labelledby="modal_popupLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document" style="margin-right:800px;" >
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user" aria-hidden="true"></i> Tambah User Associate</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
				<form id="form_add_user_associate" style="font-weight:bold">
				  {{ csrf_field() }} 
				  <div class="alert alert-warning" id="fails_add_user_assc" style="display:none">
					 
				  </div>
				  <div class="form-group">
					<label for="username">Username</label>
					<input class="form-control" id="username" name="username" required>
				  </div>
				  <div class="form-group">
					<label for="company_name">Zona</label>
					<select class="form-control" name="zone_id" id="zone_id" required>
					   <option value="">== Pilih Zona ==</option>
					  @foreach($get_zone['zone_to_associate'] as $zone)
					   <option value="{{$zone->id}}">{{ucwords($zone->zone_name)}}</option>
					  @endforeach
					</select>
				  </div>
				  <div class="form-group">
					<label for="password">Password</label>
					<input class="form-control" type="password" name="password" required>
				  </div>
				  <div class="modal-footer">				
					<button type="submit" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Tambah User Associate</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				  </div>
				</form>
			  </div>
			</div>
		  </div>
</div>

@endif

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
<input type="hidden" name="_token" value="{{ csrf_token() }}">
 @yield('content')
<script type="text/javascript" src="{{asset('assets/lib/function_addition.js')}}"></script>	
<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery.ui.touch-punch.min.js')}}"></script>	
<script type="text/javascript" src="{{asset('assets/js/HoldOn.min.js')}}"></script> 
@yield('content_js')
@yield('content_include')
</body>
</html>
