@extends('layout.layout')
@section('content')
<style>
.kiri_dikit{margin-right:770px}
</style>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="width:1350px">
<div class="form-group row" style="margin-bottom:-1px;">
	<input type="text" id="zone_title" style="font-size:28px;text-align:center;height:80px;margin-left:-114px;width:666px;font-family:sans-serif" readonly="readonly" class="form-control" value="{{ucwords($get_zone['zone_name'])}}">
	<div class="col-xs-9">
	<button type="submit" style="font-size:28px;text-align:center;height:80px;margin-left:-130px;width:135px;font-family:sans-serif;display:none" class="btn btn-Success" id="ubah_title">Ubah</button>
	</div>
</div>
<div id="table_1" style="float:left;margin-left:-130px;border:10px solid black;">
@yield('content_admin')
</div>
<script>
  var zone = {{$get_zone['zone_id']}};
</script>
<br/>
@endsection
@section('content_js')
 <script type="text/javascript" src="{{asset('assets/lib/function_Admin.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/lib/timer.js')}}"></script> 
@endsection
@section('content_include')
@include('layout.footer_dev')
@endsection