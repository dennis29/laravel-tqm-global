<div style="float:left;margin-left:5px;margin-top:-23px">
<section id="tabs">
	<div class="container" id="contains" style="margin-top:-60px;width:900px">
		<div class="row">
		    <div style="margin-right:5px;border:5px solid black;">
				<nav><center><label for="table_type"><h4>Label Management</h4></label></center>
					<div class="nav nav-tabs nav-fill">
						<a class="nav-item nav-link active" id="nav-add-tab" data-toggle="tab" href="#nav-add" role="tab" aria-controls="nav-add" aria-selected="true">Tambah Penanda</a>
					</div>
				</nav>
				<div class="tab-content py-3 px-3 px-sm-0">
					<div class="tab-pane fade show active">
					<form class="form-horizontal" id='form_add'>
						  <div class="form-group">
							<label class="control-label col-sm-7" for="label_name">Nama</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" id="label_name" placeholder="Enter Label Name">
							</div>
						  </div>
						</form>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-primary" id="add_div" onclick="add_new_label()">Tambah Penanda</button>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div style="border:5px solid black;">
				<nav><center><label for="table_type"><h4>Table Management</h4></label></center>
					<div class="nav nav-tabs nav-fill" style="width:auto" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-add-tab" data-toggle="tab" href="#nav-add" role="tab" aria-controls="nav-add" aria-selected="true">Tambah</a>
						<a class="nav-item nav-link" id="nav-del-tab" data-toggle="tab" href="#nav-del" role="tab" aria-controls="nav-del" aria-selected="false">Hapus</a>
						<a class="nav-item nav-link" id="nav-merge-tab" data-toggle="tab" href="#nav-merge" role="tab" aria-controls="nav-merge" aria-selected="false">Merge</a>
						<a class="nav-item nav-link" id="nav-split-tab" data-toggle="tab" href="#nav-split" role="tab" aria-controls="nav-split" aria-selected="false">Split</a>
					</div>
				</nav>
				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
					<div class="tab-pane tab-dm fade show active" id="nav-add" role="tabpanel" aria-labelledby="nav-add-tab">
					<form class="form-horizontal" id='form_add'>
					      <div class="form-group col-sm-10">
							  <label for="table_type">Pilih Jenis Meja :</label>
							  <select class="form-control" id="table_type">
								<option value="Circle_two">Meja bulat 2 bangku</option>
								<option value="Circle_four">Meja bulat 4 bangku</option>
								<option value="Square_two">Meja kotak 2 bangku</option>
								<option value="Square_four">Meja kotak 4 bangku</option>
							  </select>
						  </div>
						  <div class="form-group">
							<label class="control-label col-sm-7" for="table_no">Meja Nomor</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" id="table_no" placeholder="Masukkan Meja Nomor">
							</div>
						  </div>
						</form>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-primary" id="add_div" onclick="add_new_div()">Tambah</button>
							</div>
						</div>

					</div>
					<div class="tab-pane tab-dm fade" id="nav-del" role="tabpanel" aria-labelledby="nav-del-tab">
						<form class="form-horizontal" id='form_del'>
						  <div class="form-group">
							<label class="control-label col-sm-7" for="table_no">Table No</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" id="table_no" placeholder="Enter Table No">
							</div>
						  </div>
						</form>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-primary" id="del_div" onclick="delete_div()">Hapus</button>
							</div>
						</div>
					</div>
					<div class="tab-pane tab-dm fade" id="nav-merge" role="tabpanel" aria-labelledby="nav-merge-tab">
					<form class="form-horizontal" id='form_merge'>
						  <div class="form-group">
							<label class="control-label col-sm-12" for="tables_no">Masukkan Nomor Meja (dipisah dengan koma)</label>
							<div class="col-sm-12">
							  <input type="text" class="form-control" id="tables_no" value="" placeholder="Enter Tables">
							</div>
						  </div>
						</form>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-primary" id="mer_div" onclick="merge_div()">Gabung</button>
							</div>
						</div>
				    </div>
					<div class="tab-pane tab-dm fade" id="nav-split" role="tabpanel" aria-labelledby="nav-split-tab">
					<form class="form-horizontal" id='form_split'>
						  <div class="form-group">
							<label class="control-label col-sm-12" for="tables_no">Masukkan Nomor Meja di meja tergabung (dipisah dengan koma)</label>
							<div class="col-sm-12">
							  <input type="text" class="form-control" id="tables_split" value="" placeholder="Enter Tables">
							</div>
						  </div>
						</form>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-primary" id="spl_div" onclick="split_div()">Split</button>
							</div>
						</div>
				    </div>
				</div>

			</div>
		</div>
	</div>
</section>
</div>

<script>

function change_color(id="")
{
	if(document.getElementById(id).className  == "border_red")
	{
		document.getElementById(id).className  = "border_green";
	}
	else if(document.getElementById(id).className  == "border_green")
	{
		document.getElementById(id).className  = "border_blue";
	}
	else
	{
		document.getElementById(id).className  = "border_red";
	}
	
	save_body();

}

var check_add = 0;
var style_x = "";
function add_new_div(id="",table_type=""){
	var tbl_type = table_type;
	if(!id)
	{
	   var id = $('#form_add #table_no').val();
	}
	if(!tbl_type)
	{
	  tbl_type = $('#table_type').val();
	}
	if(check_add > 0){
		style_x = 'style="margin-top:-60px"';
	}
	$('#new_table').append('<div id="t'+id+'" '+style_x+' ondblclick="" ondrag="on_drag()" class="green"><img src="assets/images/'+tbl_type+'_green.jpg" width="78" height="68" class='+JSON.stringify(tbl_type)+' /><b>'+id+'</b></div>');
	$("#t"+id).draggable();
	$('#t'+id).attr('ondblclick','change_new_table_col('+id+','+JSON.stringify(tbl_type)+')');
	var position = $("#t"+id).offset();
	$(window).scrollTop(position.top);	
	check_add++; 	
	
	save_body();

}

var label = 1;

function add_new_label(){
	$('#new_label').append('<div style="margin-top:-90px;"><span id="lbl'+label+'" ondrag="on_drag()" class="input-group-text" style="display:inline;text-align:center;font-weight:bold;margin-top:-90px;" id="inputGroupPrepend">'+$('#label_name').val()+'</span></div>');
	$('#lbl'+label).draggable();
	$('#lbl'+label).focus();
	var position = $('#lbl'+label).offset();
	$(window).scrollTop(position.top);	
	label++;
	save_body();
}

function change_new_table_col(id=0,table_type=""){
  if($("#t"+id).attr('class')=="green" || $("#t"+id).attr('class')=="green ui-draggable" || $("#t"+id).attr('class')=="green ui-draggable-dragging")
			{

			   $("#t"+id).attr('class','red');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_red.jpg" width="78" height="68" class="'+table_type+'" alt=""><b>'+id+'</b>');


			}
			else if($("#t"+id).attr('class')=="red" || $("#t"+id).attr('class')=="red ui-draggable" || $("#t"+id).attr('class')=="red ui-draggable-dragging")
			{

			   $("#t"+id).attr('class','blue');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_blue.jpg" width="78" height="68" class="'+table_type+'"  alt=""><b>'+id+'</b>');

			}
			else{

			   $("#t"+id).attr('class','green');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_green.jpg" width="78" height="68" class="'+table_type+'"  alt=""><b>'+id+'</b>');
			}
			
			save_body();
			


}

function delete_div(){

	var id = $('#form_del #table_no').val();
	var img = $('#t'+id).find("img");
	$('#t'+id).html('<div style="width:78px;height:68px">&nbsp</div>');
	$('#t'+id).attr('id','');
	save_body();
}

function on_drag(id){
	save_body();
}

function split_div(){
	var arr = $('#tables_split').val().split(',');
	
	i=0;
	
	var id_div = arr.join('_');	
	
	$('#t'+id_div+'_merge img').each(function() {
	    add_new_div(arr[i],$(this).attr('class'));
		//return false;
		i++;
    });

	$('#t'+id_div+'_merge').html('<div style="width:174px;height:68px">&nbsp</div>');
	$('#t'+id_div+'_merge').attr('id','');
	
	save_body();
}


//localStorage[zone] = "";

function merge_div(){

	var arr = $('#tables_no').val().split(',');
	
	var arr_table_type = [];

	$('#tables_no').val('');

	var width = 0;

	var div="";

	for(i=0;i<arr.length;i++)
	{
		    //console.log($('#t'+arr[i]+' img').attr('class'));
	        arr_table_type[i] = $('#t'+arr[i]).children('img').attr('class');  
	        div +='<div style="float:left;" ><img class="'+	[i]+'" src="assets/images/'+$('#t'+arr[i]+' img').attr('class')+'_green.jpg" width="78" height="68" alt=""><b>'+arr[i]+'</b></div>';			
			$("#t"+arr[i]).html('<div style="width:78px;height:68px">&nbsp</div>');
			$("#t"+arr[i]).attr('id','');
	}
	var id_div = arr.join('_');

	$('#table_merge').append('<div class="green" ondrag="on_drag()" style="margin-bottom:-65px" id="t'+id_div+'_merge" ondblclick=""><b>'+div+'</b></div>');	
	
	$('#t'+id_div+'_merge').attr('ondblclick',"change_color_dm(["+arr+"],"+JSON.stringify(arr_table_type)+")");

	$('#t'+id_div+'_merge').draggable();
	
	var position = $('#t'+id_div+'_merge').offset();
	$(window).scrollTop(position.top);	
	
	save_body();

}

function change_color_dm(arr=[],arr_table_type=[]){
   var div = "";
   var id_div = arr.join('_');
   if($('#t'+id_div+'_merge').attr('class')=="green" || $('#t'+id_div+'_merge').attr('class')=="green")
   {
		      $('#t'+id_div+'_merge').attr('class','blue');
		      for(i=0;i<arr.length;i++)
			  {
				  //console.log(arr_table_type[i]);
				div +='<div style="float:left;"><img class='+JSON.stringify(arr_table_type[i])+' src="assets/images/'+arr_table_type[i]+'_blue.jpg" width="78" height="68" alt=""><b>'+arr[i]+'</b></div>';
			  }
   }
   else if($('#t'+id_div+'_merge').attr('class')=="blue" || $('#t'+id_div+'_merge').attr('class')=="blue")
   {
		     
		      $('#t'+id_div+'_merge').attr('class','red');
		      for(i=0;i<arr.length;i++)
			  {
				div +='<div style="float:left;"><img class='+JSON.stringify(arr_table_type[i])+' src="assets/images/'+arr_table_type[i]+'_red.jpg" width="78" height="68" alt=""><b>'+arr[i]+'</b></div>';
			  }
			  
   }
   else
   {
		      $('#t'+id_div+'_merge').attr('class','green');
		      for(i=0;i<arr.length;i++)
			  {
				div +='<div style="float:left;"><img class='+JSON.stringify(arr_table_type[i])+' src="assets/images/'+arr_table_type[i]+'_green.jpg" width="78" height="68" alt=""><b>'+arr[i]+'</b></div>';
			  }
   }	   
   $('#t'+id_div+'_merge').html(div);
   save_body();  

}

function div_click(id){
	if($("#t"+id).attr('class')=="border_blue rectangle")
	{

	   $("#t"+id).attr('class','border_red rectangle');

	}
	else if($("#t"+id).attr('class')=="border_green rectangle")
	{

	   $("#t"+id).attr('class','border_blue rectangle');

	}
	else{
	   $("#t"+id).attr('class','border_green rectangle');
	}
	
	save_body();
	
		

}
</script>