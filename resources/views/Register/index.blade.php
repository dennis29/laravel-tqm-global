<!DOCTYPE html>
<html lang="en">
<?php
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Pragma: no-cache');
header('Expires: 0');
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/dennis.css')}}">
<body>
    <div class="container">
        <div class="row">
			<div class="col-md-5 mx-auto">
			<div id="first">
				<div class="myform form ">
					 <div class="logo mb-3">
						 <div class="col-md-12 text-center">
							<h1>Registrasi</h1>
						 </div>
					</div>
                   <form action="{{route('to_register')}}" method="post">
				           {{ csrf_field() }} 
                           <div class="form-group">
                              <label for="username">Username</label>
                              <input type="text" name="username"  class="form-control" placeholder="Masukkan Username" required>
                           </div>
						   <div class="form-group">
                              <label for="company_name">Nama Perusahaan</label>
                              <input type="text" name="company_name" class="form-control" placeholder="Masukkan Perusahaan" required>
                           </div>
                           <div class="form-group">
                              <label for="password">Password</label>
                              <input type="password" name="password" id="password"  class="form-control" placeholder="Masukkan Password" required>
                           </div>
						    <div class="form-group" style="margin-bottom:50px">
                              <p class="text-center" style="color:#b10b0b">
							  @if($errors->any())
								{{$errors->first()}}
							  @endif
							  </p>
                           </div>
                           <div class="col-md-12 text-center">
                              <button type="submit" class=" btn btn-block mybtn btn-primary tx-tfm">Registrasi</button>
                           </div>
                           <div class="col-md-12 ">
                           </div>
                           </div>
                           <div class="form-group">
                              <p class="text-center" style="color:aquamarine">Sudah punya Username dan Password? <a href="{{url('')}}" style="color:#08ff00"><b>Login Disini</b></a></p>
                           </div>
						   @include('layout.footer_help')
                        </form>
                 
				</div>
			</div>
			</div>
		</div>
      </div>  
</body>
</html>