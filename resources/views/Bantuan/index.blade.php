<!DOCTYPE html>
<html lang="en">
<?php
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Pragma: no-cache');
header('Expires: 0');
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/dennis.css')}}">

<body>
    <div class="container">
        <div class="row">
			<div class="col-md-5 mx-auto">
			<div id="first">
				<div class="myform form ">
					 <div class="logo mb-3">
						 <div class="col-md-12 text-center">
							<h1>Bantuan</h1>
						 </div>
					</div>
                   <form action="{{route('to_login')}}" method="post">
				           {{ csrf_field() }} 
                           <div class="form-group">
                              <label>Hubungi Whatsapp :</label>
                              <h4>+6281286949932<h4>
                           </div>
                           <div class="form-group">
                              <label>Hubungi Nomor Handphone :</label>
                              <h4>+6288809615182<h4>
                           </div>
                     </form>
				</div>
			</div>
			</div>
		</div>
      </div>   
</body>
</html>